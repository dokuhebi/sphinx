.. GitLab Pages with Sphinx documentation master file, created by
   sphinx-quickstart on Thu Jan  9 10:28:38 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

This is the second page
=======================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

Second Page
============

This is a test to see how a separate section is added to sphinx.

* :ref:`genindex`
* :ref:`search`

* https://gitlab.com/pages/sphinx
